# frozen_string_literal: true

class CreateChats < ActiveRecord::Migration[5.2]
  def change
    create_table :chats do |t|
      t.references :user, foreign_key: true, index: true
      t.integer :friend_id
      t.text :message
      t.date :read_at

      t.timestamps
    end
  end
end
