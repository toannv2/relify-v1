# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.references :user, foreign_key: true, index: true
      t.column :activity_id, :bigint
      t.column :type, :tinyint
      t.date :read_at

      t.timestamps
    end
  end
end
