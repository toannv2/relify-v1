## Checklist cho MR
Tick vào trong checklist các mục đã làm. PM xác nhận pass hết các điều kiện dưới đây thì nhấn approve

### [Developer]
  - [ ] Đã tự review lại source code của mình trước khi nhờ người khác review
  - [ ] Đã đặt Title MR và commit message theo định dạng `Task #{ISSUE_NUMBER} {ISSUE_CONTENTS}`
    - Trong đó :
      - `#{ISSUE_NUMBER}` là Redmine ID.
      - `#{ISSUE_CONTENTS}` là nội dung ngắn gọn mô tả task
    - V/d: `Task #1234 Hôm nay trời nhẹ lên cao`
  - [ ] Đã self-test và đạt tất cả các tiêu chuẩn nghiệm thu của task
  - [ ] Đã viết đầy đủ thông tin trong Redmine. Mục nào không có thì ghi "Không", không để giá trị mặc định
  - [ ] Đã chuyển Redmine issue sang trạng thái Resolved (Coding done)

### [Reviewer]
  - [ ] Đã kiểm tra Redmine các mục: MÔ TẢ CÁCH LÀM THỰC TẾ, PHẠM VI ẢNH HƯỞNG, TRẠNG THÁI ISSUE...
  - [ ] Đã review source code và xác nhận `KHÔNG CÓ LỖI VỀ MẶT CÚ PHÁP`
  - [ ] Đã review source code và xác nhận `CHẠY ĐÚNG NHƯ YÊU CẦU, KHÔNG THỪA KHÔNG THIẾU`
  - [ ] Đã review source code và xác nhận `KHÔNG GÂY SAI LỆCH TỚI CHỨC NĂNG KHÁC`

### [Tester]
  - [ ] Đã đọc và kiểm tra kết quả self-test của Developer
  - [ ] Đã test lại và xác nhận KHÔNG GÂY ẢNH HƯỞNG TỚI CHỨC NĂNG KHÁC
  - [ ] Đã chuyển Redmine issue sang trạng thái Test done
  - [ ] Đã điền link Checklist vào trong Redmine

### [PM]
  - [ ] Xác nhận Release Notes không có vấn đề gì
