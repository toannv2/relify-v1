# frozen_string_literal: true

class UserMailer < ApplicationMailer
  default from: 'thuyhh@relipasoft.com'
  def account_activation(user)
    @user = user
    mail to: user.email, subject: 'Account activation'
  end
end
