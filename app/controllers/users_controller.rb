# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update new index create]
  skip_before_action :verify_authenticity_token

  skip_before_action :loginRequired

  def index
    # page = params[:page] ? params[:page] : 1;

    @users = User.all
    respond_to do |format|
      # format.html { render :index }
      format.json { render json: @users }
    end
  end

  def show
    user = User.where(sex: params[:sex])
    respond_to do |format|
      format.html { render :show }
      format.json { render json: @users }
    end
    # render json: @user
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new user_params
    respond_to do |format|
      if @user.save
        UserMailer.account_activation(@user).deliver_now
        format.html { redirect_to root_url }
        format.json {}
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }

      end
    end
  end

  def update
    if current_user.id === @user.id
      if @user.update_attributes user_params
        flash[:notice] = 'Edit information Successfully'
        redirect_to user_path
      else
        flash[:alert] = 'Edit information failed'
        redirect_to edit_user_path
        end
    else
      flash[:alert] = 'You do not have permission to edit'
      redirect_to edit_user_path(current_user)
    end
  end

  private

  def set_user
    @user = User.find_by id: params[:id]
  end

  def user_params
    params.require(:user).permit :first_name, :last_name, :email, :password, :password_confirmation,
                                 :birth, :sex, :introduction, :role, :city, :country
  end
end
