# frozen_string_literal: true

class NewsfeedsController < ApplicationController
  def index
    @post = Post.new
    @image = @post.images.build
  end

  def list_image
    @image = Image.new
  end
end
